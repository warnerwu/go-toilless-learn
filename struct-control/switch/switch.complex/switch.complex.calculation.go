package main

import "fmt"

func main() {
	// defined variable score in the inside
	score := 59

	// switch branch matching
	switch score / 10 {
	case 10:
	case 9:
		fmt.Println("优秀")
	case 8:
		fmt.Println("良好")
	case 7:
		fmt.Println("一般")
	case 6:
		fmt.Println("及格")
	default:
		fmt.Println("不及格")
	}
}
