package main

import "fmt"

func main() {
	// defined variable score in the inside
	score := 81

	if score >= 90 && score <= 100 {
		fmt.Println( "优秀" )
	} else if score >= 80 && score < 90 {
		fmt.Println( "良好" )
	} else if score >= 70 && score < 80 {
		fmt.Println( "一般" )
	} else if score >= 60 && score < 70 {
		fmt.Println( "及格" )
	} else {
		fmt.Println( "不及格" )
	}
}
