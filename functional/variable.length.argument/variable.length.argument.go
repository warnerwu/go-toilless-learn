package main

import "fmt"

func main() {
	// 通过可变长参数函数sum求和
	sum := sum(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)

	// 输出结果
	fmt.Println(sum)	// 55

	// 通过可变长参数函数sum求和, 但包含基数
	sumVariableLengthArgumentValue := sumVariableLengthArgument(100, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10)

	// 输出结果
	fmt.Println(sumVariableLengthArgumentValue)	// 155

	// #################################################################################################################
	// 注意：这里不知道你是否觉得这个例子其实和那个切片的例子很像啊，在哪里呢?
	// #################################################################################################################

	// 切片定义并初始化
	var slices = []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}

	// 呵呵，就是把切片“啪，啪，啪”三个耳光打碎了，传递过去啊!:-P
	sumVariableLengthArgumentBaseAndSum := sumVariableLengthArgument(200, slices...)

	// 输出结果
	fmt.Println(sumVariableLengthArgumentBaseAndSum)	// 255

}

/**
	变长参数

	@arr []int 虚参 arr 类型为 []int 切片
 */
func sum(arr ...int) (sum int) {
	// 求和
	for _, elem := range arr {
		sum += elem
	}
	// 通过返回值命名参数返回
	return
}

/**
	变长参数, 如果函数包含多个参数

	另外还有一点需要注意，那就是可变长参数定义只能是函数的最后一个参数。
	
	比如下面的例子
 */
func sumVariableLengthArgument(base int, arr ...int) int {
	sum := base
	for _, elem := range arr {
		sum += elem
	}
	return sum
}