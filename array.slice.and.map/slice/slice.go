package main

import "fmt"

func main() {
	// 切片有两种定义方式:
	//
	// 一种是先声明一个变量是切片，然后使用内置函数 make 去初始化这个切片。
	// 另外一种是通过取数组切片来赋值。

	// -----------------------------------------------------------------------------------------------------------------
	// 切片定义方式一: 先声明一个变量是切片，然后使用内置函数 make 去初始化这个切片
	// -----------------------------------------------------------------------------------------------------------------

	// 先声明一个变量是切片，然后使用内置函数 make 去初始化这个切片
	var x = make([]float64, 5)	// 指定切片长度(length)
	var y = make([]float64, 5, 10)	// 指定切片长度(length)及容量(capacity)

	// 输出切片x的容量(capacity)及长度(length)值, 以及切片值
	fmt.Println("Capacity:", cap(x), ", 	length: ", len(x), ", current slice value is ", x)
	// 输出切片y的容量(capacity)及长度(length)值, 以及切片值
	fmt.Println("Capacity:", cap(y), ", 	length: ", len(y), ", current slice value is ", y)

	// 对切片赋值操作
	for i := 0; i < len(x); i++ {
		x[i] = float64(i+1)
	}

	for i := 0; i < len(y); i++ {
		y[i] = float64(i+1)
	}

	// 输出切片x的容量(capacity)及长度(length)值, 以及切片值
	fmt.Println("Capacity:", cap(x), ", 	length: ", len(x), ", current slice value is ", x)
	// 输出切片y的容量(capacity)及长度(length)值, 以及切片值
	fmt.Println("Capacity:", cap(y), ", 	length: ", len(y), ", current slice value is ", y)


	// -----------------------------------------------------------------------------------------------------------------
	// 切片定义方式二: 通过数组切片赋值
	//
	// 采用[lowIndex:highIndex]的方式获取数值切片, 其中切片元素包括lowIndex的元素, 但是不包括highIndex的元素
	// -----------------------------------------------------------------------------------------------------------------

	// 定义数组
	var arr = []int{1, 2, 3, 4, 5}
	s1 := arr[2:3]	// [3]
	s2 := arr[:3]	// [1, 2, 3]
	s3 := arr[2:]	// [3, 4, 5]
	s4 := arr[:]	// [1, 2, 3, 4, 5]

	// 打印切片元素
	fmt.Println(s1)
	fmt.Println(s2)
	fmt.Println(s3)
	fmt.Println(s4)
}
