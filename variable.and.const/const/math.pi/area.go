package main

import (
	"math"
	"fmt"
)

func main() {
	// 定义半径值
	const radius float64 = 10

	// 计算圆的面积
	area :=  math.Pi * math.Pow( radius, 2 )

	fmt.Println( area )
}
