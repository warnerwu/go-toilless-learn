package common

import (
	"time"
)


/**
	转递时间戳返回格式化日期时间字符串

	@author 武邵华 warnerwu@126.com
	@time 2018-03-31 01:19:35
 */
func TimeString(timestamp int64) (string) {
	return time.Unix(timestamp, 0).Format("2006-01-02 03:04:05 PM")
}
/**
	获取当前时间戳

	@author 武邵华 warnerwu@126.com
	@time 2018-03-31 01:19:35
 */
func Timestamp() (timestamp int64) {
	return time.Now().Unix()
}