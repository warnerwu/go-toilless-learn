package main

import (
	"fmt"
	"../common"
)


func main() {

	// 调用自制包方法, 获取时间戳
	timestamp := common.Timestamp()

	// 输出当前间戳
	fmt.Printf("current timestamp is: %d\r\n", timestamp)

	// 调用自制包方法, 获取时间格式化字符串
	timeString := common.TimeString(timestamp)

	// 输出当前时间及unix类型
	fmt.Printf("当前时间为：%s", timeString)
}



