package main

import "fmt"

func main() {
	// 现在我们再来看看 Go 提供的内置函数 delete，这个函数可以用来从字典中删除元素

	// 定义并初始化字典
	nets := make(map[string]string)

	// 字典赋值操作
	nets["alibaba"] = "阿里巴巴"
	nets["baidu"] = "百度"
	nets["qq"] = "腾讯"
	nets["wumei"] = "物美"
	nets["tuhu"] = "途虎"
	nets["meituan"] = "美团"

	// 删除前打印字典长度及字典内容
	// Before delete: length is 6, map is map[alibaba:阿里巴巴 baidu:百度 qq:腾讯 wumei:物美 tuhu:途虎 meituan:美团]
	fmt.Printf("Before delete: length is %d, map is %s\r\n", len(nets), nets)

	// 删除字典元素
	delete(nets, "qq")

	// 删除后打印字典长度及字典内容
	// Before delete: length is 5, map is map[baidu:百度 wumei:物美 tuhu:途虎 meituan:美团 alibaba:阿里巴巴]
	fmt.Printf("Before delete: length is %d, map is %s\r\n", len(nets), nets)
}
