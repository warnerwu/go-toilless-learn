package main

import (
	"fmt"
	"sort"
)
func main() {
	// 字典的定义也有两种，
	//
	// 一种是初始化数据的定义方式
	// 另一种是使用神奇的 make 函数来定义


	// -----------------------------------------------------------------------------------------------------------------
	// 字典定义方式一: 初始化数据的定义方式
	// -----------------------------------------------------------------------------------------------------------------

	// 初始化数据的方式定义字典
	fruits := map[string]string {
		"A": "Apple",
		"B": "Banana",
		"O": "Orange",
		"P": "Pear",
	}

	// 通过内置函数range遍历数组
	for k, v := range fruits {
		fmt.Printf("map key is: %s, value is %s\r\n", k, v)
	}

	// 添加空行
	fmt.Println()

	// -----------------------------------------------------------------------------------------------------------------
	// 字典定义方式二: 使用神奇的 make 函数来定义
	// -----------------------------------------------------------------------------------------------------------------

	// map变量定义
	var nets map[string]string

	// map初始化
	nets = make(map[string]string)

	// map赋值操作
	nets["alibaba"] = "阿里巴巴"
	nets["baidu"] = "百度"
	nets["qq"] = "腾讯"
	nets["wumei"] = "物美"
	nets["tuhu"] = "途虎"
	nets["meituan"] = "美团"

	// 遍历字典
	for k, v := range nets {
		fmt.Printf("map key is: %s, value is %s\r\n", k, v)
	}

	// 添加空行
	fmt.Println()


	// #################################################################################################################
	// 注意：当然上面的例子中，我们可以把定义和初始化合成一句
	// #################################################################################################################

	// map的定义并初始化
	company := make(map[string]string)

	// map赋值操作
	company["alibaba"] = "阿里巴巴"
	company["baidu"] = "百度"
	company["qq"] = "腾讯"
	company["wumei"] = "物美"
	company["tuhu"] = "途虎"
	company["meituan"] = "美团"

	// 定义排序切片
	sortSlice := make([]string, 0)

	// 遍历字典保存字典的键到切片
	for k, _ := range company {
		sortSlice = append(sortSlice, string(k))
	}

	// 对切片排序操作
	sort.Strings(sortSlice)

	// 输出排序后的字典键值对
	for _, elem := range sortSlice {
		if _, ok := company[elem]; ok {
			fmt.Printf("map key is: %s, value is %s \r\n", elem, company[elem])
		}
	}
}

