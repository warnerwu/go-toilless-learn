package main

import "fmt"

func main() {
	const x  string = "hello world!";
	fmt.Println( x )

	// x = "i love go language";
	// fmt.Println( x )

}
