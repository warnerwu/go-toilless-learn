package random

import (
	"time"
	"math"
	"math/rand"
)

/**
	返回指定个数及长度的随机数切片

	@number int 返回随机数的个数
	@len float64 随机数长度
	@author 武邵华 warnerwu@126.com
	@time 2018-03-31 19:29:35
 */
func Random(number int, len float64) ( res []int ) {
	// 获取随机数
	r := rand.New( rand.NewSource( time.Now().UnixNano() ) )

	// 随机数长度
	len = math.Pow( 10, len )

	// 添加指定个数字的随机数到结果切片
	for i := 0; i < number; i++ {
		// 将随机数添加到结果切片
		res = append( res, r.Intn( int(len) ) )
	}

	// 返回结果切片
	return res
}