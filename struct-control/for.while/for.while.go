package main

import "fmt"

func main() {
	i := 1
	sum := 0
	for i <= 100 {
		sum += i
		fmt.Printf( "while实现1...100之间自然数累加, 第%d次i为%d, sum的和为%d\r\n", i, i, sum )
		i++
	}
}
