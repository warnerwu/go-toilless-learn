package main

import (
	"fmt"
)

func main() {
	// 多维字典的简单使用

	// 最后我们再用一个稍微复杂的例子来结束字典的介绍

	// 我们有一个学生登记表，登记表里面有一组学号，每个学号对应一个学生，每个学生有名字和年龄

	// 定义并初始化字典
	facebook := make(map[string]map[string]int)

	// 字典赋值操作
	facebook["1522588698488409907"] = map[string]int{"liu": 22}
	facebook["1522588698488410381"] = map[string]int{"wu": 15}
	facebook["1522588698488410575"] = map[string]int{"zhang": 18}
	facebook["1522588698488410716"] = map[string]int{"tian": 15}
	facebook["1522588698488410752"] = map[string]int{"niu": 12}

	// 输出登记表学生信息
	for studentNumber, studentInfo := range facebook {
		for studentName, studentAge := range studentInfo  {
			fmt.Printf( "学号为: %s, 的学生姓名为: %s, 年龄为: %d\r\n", studentNumber, studentName, studentAge )
		}
	}

	// 输出空行
	fmt.Println()


	// #################################################################################################################
	// 注意：当然我们也可以用初始化的方式定义字典
	// #################################################################################################################

	// 初始化的方式定义字典
	sian := map[string]map[string]int {
		"20180401221810": map[string]int{"warnerwu": 22},
		"20180401221811": map[string]int{"tina": 25},
		"20180401221812": map[string]int{"king": 88},
		"20180401221813": map[string]int{"ketty": 23},
		"20180401221814": map[string]int{"lamando": 12},
		"20180401221815": map[string]int{"warnerwu": 28},
	}

	// 输出登记表学生信息
	// 学号为: 20180401221810, 的学生姓名为: warnerwu, 年龄为: 22
	// 学号为: 20180401221811, 的学生姓名为: tina, 年龄为: 25
	// 学号为: 20180401221812, 的学生姓名为: king, 年龄为: 88
	// 学号为: 20180401221813, 的学生姓名为: ketty, 年龄为: 23
	// 学号为: 20180401221814, 的学生姓名为: lamando, 年龄为: 12
	// 学号为: 20180401221815, 的学生姓名为: warnerwu, 年龄为: 28
	for studentNumber, studentInfo := range sian {
		for studentName, studentAge := range studentInfo  {
			fmt.Printf( "学号为: %s, 的学生姓名为: %s, 年龄为: %d\r\n", studentNumber, studentName, studentAge )
		}
	}
}
