package main

import "fmt"

func main() {
	// 调用函数返回切片元素的和以及平均值
	sum, avg := sliceSum([]int{1, 2, 3, 4, 5, 6, 7, 8, 9})

	// 打印调用函数返回切片元素的和以及平均值
	fmt.Printf("slice elements sum value is :%d, slice elements avg value is %f\r\n", sum, avg)
}


/**
	命令返回值
 */
func sliceSum(arr []int) (sum int, avg float64)  {
	// 求和
	for _, elem := range arr {
		sum += elem
	}
	// 求平均值
	avg = float64(sum) / float64(len(arr))

	// 通过返回值命名参数返回
	return
}
