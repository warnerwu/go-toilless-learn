### GO轻松学

#### 数组定义

- 方式一

```go
var arr [5]int  // 定义长度为5类型为int的数组arr
```

- 方式二

```go
var arr =  [5]int{1, 2, 3, 4, 5}    // 定义长度为5类型为int的数组arr并初始化
```

- 方式三

```go
var arr2 = [5]int{} // 定义长度为5类型为int的数组
```
- 方式四

```go
var week2 = [...]string{
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday",
    "Sunday",
}
```
#### 切片定义

- 方式一

```go
var slices = make([]int, 5)    // 定义切片并初始化切片长度为5
```

- 方式二

```go
var slices = make([]int, 5, 10)    // 定义切片并初始化切片长度为5容量10
```

- 方式三

```go
// 数组
var arr = [10]int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}

// 通过数组切片赋值
var slice = arr[4:]
```

- 方式四

```go
slices := []int{1, 2, 3, 4, 5, 6}
```

##### 切片追加元素

```go
slices1 = append(slices1, 5, 6, 7, 8, 9, 10)
```

##### 切片拷贝元素

```go
copy(dst, src)
```

#### 字典定义

- 方式一

```go
// 初始化数据的方式定义字典
fruits := map[string]string {
    "A": "Apple",
    "B": "Banana",
    "O": "Orange",
    "P": "Pear",
}
```

- 方法二

```go
// 使用make内置函数定义并初始化字典
fruits := make(map[string]string)
```

##### 字典元素删除

```go
delete(map, key)
```
