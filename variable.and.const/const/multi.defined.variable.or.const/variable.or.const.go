package main

import "fmt"

var (
	a, b, c = 1, 2, 3
)

const (
	PI float64 = 3.14
	ADMIN = "warnerwu"
)

func main() {
	fmt.Println( a, b, c )
	fmt.Println( PI, ADMIN )
}
