package main

import "fmt"

func main() {
	// switch 的判断条件可以为任何数据类型
	dogSex := 'F'
	switch dogSex {
	case 'M':
		fmt.Printf("a male dog, type is %T\r\n", dogSex)
	case 'F':
		fmt.Printf("a female dog, type is %T\r\n", dogSex)
	}
}
