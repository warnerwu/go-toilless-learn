package main

import (
	"fmt"
	"time"
)

func main() {
	for {
		// 获取时间戳
		timestamp := time.Now().Unix()

		// 无限输出当前时间戳
		fmt.Println( timestamp )
	}
}
