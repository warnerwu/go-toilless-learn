package dump

import "fmt"

/**
	打印切片结构

	@slices []int 待输出打印结构切片
	@len float64 随机数长度
	@author 武邵华 warnerwu@126.com
	@time 2018-03-31 19:29:35
 */
func DSliceStructure(slices []int) (int, error) {
	return fmt.Printf("slices capacity is: %d, length is: %d, value elements is: %d\r\n", cap(slices), len(slices), slices)
}