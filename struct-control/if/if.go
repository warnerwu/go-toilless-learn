package main

import "fmt"

// defined const in the global
const Male = 'M'
const Female = 'F'

func main() {
	// defined variable in the inside
	dogAge := 10
	dogSex := 'M'

	if dogAge == 10 && dogSex == Male {
		fmt.Println("a big dog")
	}
}
